package ar.fiuba.tdd.tp0;

import java.util.Optional;
import java.util.Stack;

public class StackNumbers {

	private Stack<Float> stackNumbers;

	public StackNumbers() {
		stackNumbers = new Stack<Float>();
	}

	public void push(float number) throws IllegalArgumentException {
		stackNumbers.push(number);
	}

	public float pop() throws IllegalArgumentException {
		Optional.ofNullable(stackNumbers).filter(y -> !y.isEmpty()).orElseThrow(IllegalArgumentException::new);
		return stackNumbers.pop();
	}

	public boolean isEmpty() {
		return stackNumbers.isEmpty();
	}

	public StackNumbers reverseStack() throws IllegalArgumentException {
		Optional.ofNullable(stackNumbers).filter(x -> !x.isEmpty()).orElseThrow(IllegalArgumentException::new);
		StackNumbers stackNumbersDiv = new StackNumbers();
		while (!stackNumbers.isEmpty()) {
			float y = stackNumbers.pop();
			stackNumbersDiv.push(y);
		}
		return stackNumbersDiv;
	}

}
