package ar.fiuba.tdd.tp0;

import java.util.Optional;
import java.util.StringTokenizer;

import ar.fiuba.tdd.tp0.Operator.Operator;

public class RPNCalculator {

	private StackNumbers stackNumbers;

	private OperatorDictionary operatorDictionary;

	public RPNCalculator() {
		operatorDictionary = new OperatorDictionary();
		stackNumbers = new StackNumbers();
	}

	public float eval(String expression) throws IllegalArgumentException {
		Optional.ofNullable(expression).orElseThrow(IllegalArgumentException::new);
		StringTokenizer tokens = new StringTokenizer(expression);
		Optional.ofNullable(tokens).filter(y -> y.hasMoreTokens()).orElseThrow(IllegalArgumentException::new);
		while (tokens.hasMoreTokens()) {
			this.parserToken(tokens.nextToken());
		}
		return stackNumbers.pop();
	}

	private void parserToken(String token) throws IllegalArgumentException {
		Operator operator = operatorDictionary.getOperator(token);
		operator.eval(stackNumbers);
	}

}
