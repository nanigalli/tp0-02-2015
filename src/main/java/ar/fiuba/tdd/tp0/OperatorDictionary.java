package ar.fiuba.tdd.tp0;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

import ar.fiuba.tdd.tp0.Operator.GetNumber;
import ar.fiuba.tdd.tp0.Operator.Operator;
import ar.fiuba.tdd.tp0.Operator.OperatorNArios;
import ar.fiuba.tdd.tp0.Operator.OperatorNAriosWithReverseStack;
import ar.fiuba.tdd.tp0.Operator.OperatorWithTwoVariables;

public class OperatorDictionary {
	private Map<String, Operator> OperatorMap;

	public OperatorDictionary() {
		OperatorMap = new HashMap<String, Operator>();
		OperatorMap.put("+", (stack) -> stack.push(stack.pop() + stack.pop()));
		OperatorMap.put("-", new OperatorWithTwoVariables((x, y) -> y - x));
		OperatorMap.put("*", (stack) -> stack.push(stack.pop() * stack.pop()));
		OperatorMap.put("/", new OperatorWithTwoVariables((x, y) -> y / x));
		OperatorMap.put("MOD", new OperatorWithTwoVariables((x, y) -> y % x));
		OperatorMap.put("++", new OperatorNArios((x, y) -> x + y));
		OperatorMap.put("--", new OperatorNAriosWithReverseStack((x, y) -> x - y));
		OperatorMap.put("**", new OperatorNArios((x, y) -> x * y));
		OperatorMap.put("//", new OperatorNAriosWithReverseStack((x, y) -> x / y));
	}

	public Operator getOperator(String operator) {
		return Optional.ofNullable(OperatorMap.get(operator)).orElse(new GetNumber(operator));
	}
}
