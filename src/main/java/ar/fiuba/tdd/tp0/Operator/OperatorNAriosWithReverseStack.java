package ar.fiuba.tdd.tp0.Operator;

import java.util.Optional;

import ar.fiuba.tdd.tp0.StackNumbers;

public class OperatorNAriosWithReverseStack implements Operator {

	private OperatorNArios operator;

	public OperatorNAriosWithReverseStack(Operation operation) {
		operator = new OperatorNArios(operation);
	}

	public void eval(StackNumbers stackNumbers) throws IllegalArgumentException {
		Optional.ofNullable(stackNumbers).orElseThrow(IllegalArgumentException::new);
		StackNumbers reverseStackNumbers = stackNumbers.reverseStack();
		operator.eval(reverseStackNumbers);
		stackNumbers.push(reverseStackNumbers.pop());
	}

}
