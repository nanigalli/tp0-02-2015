package ar.fiuba.tdd.tp0.Operator;

public interface Operation {

	public float calculate(float x, float y);

}
