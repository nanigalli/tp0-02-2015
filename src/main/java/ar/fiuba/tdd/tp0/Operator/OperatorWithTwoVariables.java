package ar.fiuba.tdd.tp0.Operator;

import java.util.Optional;

import ar.fiuba.tdd.tp0.StackNumbers;

public class OperatorWithTwoVariables implements Operator {

	private Operation operation;

	public OperatorWithTwoVariables(Operation operation) {
		Optional.ofNullable(operation).orElseThrow(IllegalArgumentException::new);
		this.operation = operation;
	}

	@Override
	public void eval(StackNumbers stackNumbers) throws IllegalArgumentException {
		float x = stackNumbers.pop();
		float y = stackNumbers.pop();
		stackNumbers.push(operation.calculate(x, y));
	}

}
