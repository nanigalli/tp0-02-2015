package ar.fiuba.tdd.tp0.Operator;

import ar.fiuba.tdd.tp0.StackNumbers;

public class OperatorNArios implements Operator {

	protected float x;
	protected Operation operation;

	public OperatorNArios(Operation operation) {
		this.operation = operation;
	}

	@Override
	public void eval(StackNumbers stackNumbers) throws IllegalArgumentException {
		this.operate(stackNumbers);
		stackNumbers.push(x);
	}

	protected void operate(StackNumbers stackNumbers) {
		x = stackNumbers.pop();
		while (!stackNumbers.isEmpty()) {
			float y = stackNumbers.pop();
			x = operation.calculate(x, y);
		}
	}

}
