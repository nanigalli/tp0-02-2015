package ar.fiuba.tdd.tp0.Operator;

import java.text.NumberFormat;
import java.text.ParseException;
import java.util.Locale;
import java.util.Optional;

import ar.fiuba.tdd.tp0.StackNumbers;

public class GetNumber implements Operator {

	private String number;

	public GetNumber(String number) {
		Optional.ofNullable(number).orElseThrow(IllegalArgumentException::new);
		this.number = number;
	}

	@Override
	public void eval(StackNumbers stackNumbers) throws IllegalArgumentException {
		NumberFormat format = NumberFormat.getInstance(Locale.US);
		try { // TODO cambiar el try - catch
			Number number = format.parse(this.number);
			float numberFloat = number.floatValue();
			stackNumbers.push(numberFloat);
		} catch (ParseException e) {
			throw new IllegalArgumentException();
		}
	}

}
