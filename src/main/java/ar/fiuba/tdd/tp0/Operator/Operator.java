package ar.fiuba.tdd.tp0.Operator;

import ar.fiuba.tdd.tp0.StackNumbers;

public interface Operator {

	public void eval(StackNumbers stackNumbers) throws IllegalArgumentException;

}
