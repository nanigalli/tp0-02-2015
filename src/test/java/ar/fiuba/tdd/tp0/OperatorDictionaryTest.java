package ar.fiuba.tdd.tp0;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import ar.fiuba.tdd.tp0.Operator.Operator;

public class OperatorDictionaryTest {

	private static final double DELTA = 0.00001;
	private final OperatorDictionary operatorDictionary = new OperatorDictionary();
	private final StackNumbers stack = new StackNumbers();

	@Test
	public void operatorGetNumber() {
		Operator operator = operatorDictionary.getOperator("5");
		operator.eval(stack);
		assertEquals(5, stack.pop(), DELTA);
	}

	@Test
	public void operatorGetNumberAndSum() {
		Operator operator = operatorDictionary.getOperator("5");
		operator.eval(stack);
		operator = operatorDictionary.getOperator("2");
		operator.eval(stack);
		operator = operatorDictionary.getOperator("+");
		operator.eval(stack);
		assertEquals(5 + 2, stack.pop(), DELTA);
	}

	@Test(expected = IllegalArgumentException.class)
	public void operatorNotExist() {
		Operator operator = operatorDictionary.getOperator("!");
		operator.eval(stack);
	}

	@Test(expected = IllegalArgumentException.class)
	public void nullOperator() {
		operatorDictionary.getOperator(null);
	}

}
