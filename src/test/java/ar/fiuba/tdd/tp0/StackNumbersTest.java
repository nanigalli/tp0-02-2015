package ar.fiuba.tdd.tp0;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class StackNumbersTest {
	
	private static final double DELTA = 0.00001;
	private final StackNumbers stack = new StackNumbers();

	@Test
	public void pushAndPopNumber() {
		stack.push(1);
		assertEquals(1, stack.pop(), DELTA);
	}

	@Test(expected = IllegalArgumentException.class)
	public void emptyStackAndPop() {
		stack.pop();
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void reverseEmptyStack() {
		stack.reverseStack();
	}
	
	@Test
	public void reverseStack() {
		stack.push(1);
		stack.push(2);
		StackNumbers stackAux = stack.reverseStack();
		float x = stackAux.pop();
		assertEquals(1, x, DELTA);
		x = stackAux.pop();
		assertEquals(2, x, DELTA);
	}

}
