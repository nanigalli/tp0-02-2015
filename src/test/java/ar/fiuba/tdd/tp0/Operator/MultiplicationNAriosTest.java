package ar.fiuba.tdd.tp0.Operator;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import ar.fiuba.tdd.tp0.StackNumbers;

public class MultiplicationNAriosTest {
	private static final double DELTA = 0.00001;
	private final OperatorNArios multiplication = new OperatorNArios((x, y) -> x * y);
	private final StackNumbers stack = new StackNumbers();

	@Test
	public void multiplication() {
		stack.push(5);
		stack.push(2);
		multiplication.eval(stack);
		assertEquals(5 * 2, stack.pop(), DELTA);
	}

	@Test
	public void multiplicationNAriosNegativeNumbers() {
		stack.push(3);
		stack.push(-2);
		multiplication.eval(stack);
		assertEquals(3 * (-2), stack.pop(), DELTA);
	}

	@Test
	public void incompleteExpression() {
		stack.push(3);
		multiplication.eval(stack);
		assertEquals(3, stack.pop(), DELTA);
	}

	@Test
	public void multiMultiplication() {
		stack.push(5);
		stack.push(2);
		stack.push(4);
		stack.push(5);
		multiplication.eval(stack);
		assertEquals(5 * 2 * 4 * 5, stack.pop(), DELTA);
	}

	@Test(expected = IllegalArgumentException.class)
	public void multiplicationNAriosWithNoArgument() {
		multiplication.eval(stack);
	}
}
