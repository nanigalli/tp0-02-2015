package ar.fiuba.tdd.tp0.Operator;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import ar.fiuba.tdd.tp0.StackNumbers;

public class SumNAriosTest {

	private static final double DELTA = 0.00001;
	private final OperatorNArios sum = new OperatorNArios((x, y) -> x + y);
	private final StackNumbers stack = new StackNumbers();

	@Test
	public void sum() {
		stack.push(5);
		stack.push(2);
		sum.eval(stack);
		assertEquals(5 + 2, stack.pop(), DELTA);
	}

	@Test
	public void sumNegativeNumbers() {
		stack.push(3);
		stack.push(-2);
		sum.eval(stack);
		assertEquals(3 - 2, stack.pop(), DELTA);
	}

	@Test
	public void incompleteExpression() {
		stack.push(3);
		sum.eval(stack);
		assertEquals(3, stack.pop(), DELTA);
	}

	@Test
	public void multiSum() {
		stack.push(5);
		stack.push(2);
		stack.push(4);
		stack.push(5);
		sum.eval(stack);
		assertEquals(5 + 2 + 4 + 5, stack.pop(), DELTA);
	}

	@Test(expected = IllegalArgumentException.class)
	public void sumWithNoArgument() {
		sum.eval(stack);
	}

}
