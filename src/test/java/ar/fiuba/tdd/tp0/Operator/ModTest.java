package ar.fiuba.tdd.tp0.Operator;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import ar.fiuba.tdd.tp0.StackNumbers;

public class ModTest {

	private static final double DELTA = 0.00001;
	private final OperatorWithTwoVariables mod = new OperatorWithTwoVariables((x, y) -> y % x);
	private final StackNumbers stack = new StackNumbers();

	@Test
	public void mod() {
		stack.push(50);
		stack.push(3);
		mod.eval(stack);
		assertEquals(50 % 3, stack.pop(), DELTA);
	}

	@Test
	public void modNegativeNumbers() {
		stack.push(-30);
		stack.push(7);
		mod.eval(stack);
		assertEquals(-30 % 7, stack.pop(), DELTA);
	}

	@Test(expected = IllegalArgumentException.class)
	public void incompleteExpression() {
		stack.push(3);
		mod.eval(stack);
	}

	@Test(expected = IllegalArgumentException.class)
	public void modWithNoArgument() {
		mod.eval(stack);
	}

}
