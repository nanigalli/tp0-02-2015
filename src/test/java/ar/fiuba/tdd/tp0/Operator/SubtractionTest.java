package ar.fiuba.tdd.tp0.Operator;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import ar.fiuba.tdd.tp0.StackNumbers;

public class SubtractionTest {

	private static final double DELTA = 0.00001;
	private final OperatorWithTwoVariables subtraction = new OperatorWithTwoVariables((x, y) -> y - x);
	private final StackNumbers stack = new StackNumbers();

	@Test
	public void subtraction() {
		stack.push(5);
		stack.push(2);
		subtraction.eval(stack);
		assertEquals(5 - 2, stack.pop(), DELTA);
	}

	@Test
	public void subtractionNegativeNumbers() {
		stack.push(3);
		stack.push(-2);
		subtraction.eval(stack);
		assertEquals(3 + 2, stack.pop(), DELTA);
	}

	/*
	 * @Test(expected = IllegalArgumentException.class) public void
	 * sumWithoutStack() { subtraction.eval(null); }
	 */

	@Test(expected = IllegalArgumentException.class)
	public void incompleteExpression() {
		stack.push(3);
		subtraction.eval(stack);
	}

	@Test(expected = IllegalArgumentException.class)
	public void subtractionWithNoArgument() {
		subtraction.eval(stack);
	}

}
