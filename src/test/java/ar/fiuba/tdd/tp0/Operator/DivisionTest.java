package ar.fiuba.tdd.tp0.Operator;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import ar.fiuba.tdd.tp0.StackNumbers;

public class DivisionTest {

	private static final double DELTA = 0.00001;
	private final OperatorWithTwoVariables division = new OperatorWithTwoVariables((x, y) -> y / x);
	private final StackNumbers stack = new StackNumbers();

	@Test
	public void division() {
		stack.push(50);
		stack.push(2);
		division.eval(stack);
		assertEquals(50 / 2, stack.pop(), DELTA);
	}

	@Test
	public void divisionNegativeNumbers() {
		stack.push(-30);
		stack.push(2);
		division.eval(stack);
		assertEquals(-30 / 2, stack.pop(), DELTA);
	}

	@Test(expected = IllegalArgumentException.class)
	public void incompleteExpression() {
		stack.push(3);
		division.eval(stack);
	}

	@Test(expected = IllegalArgumentException.class)
	public void divisionWithNoArgument() {
		division.eval(stack);
	}

}
