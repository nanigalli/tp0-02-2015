package ar.fiuba.tdd.tp0.Operator;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import ar.fiuba.tdd.tp0.StackNumbers;

public class DivisionNAriosTest {
	private static final double DELTA = 0.00001;
	private final OperatorNAriosWithReverseStack division = new OperatorNAriosWithReverseStack((x, y) -> x / y);
	private final StackNumbers stack = new StackNumbers();

	@Test
	public void division() {
		stack.push(50);
		stack.push(2);
		division.eval(stack);
		assertEquals(50 / 2, stack.pop(), DELTA);
	}

	@Test
	public void divisionNAriosNegativeNumbers() {
		stack.push(-30);
		stack.push(2);
		division.eval(stack);
		assertEquals(-30 / 2, stack.pop(), DELTA);
	}

	@Test(expected = IllegalArgumentException.class)
	public void divisionNAriosWithoutStack() {
		division.eval(null);
	}

	@Test
	public void incompleteExpression() {
		stack.push(3);
		division.eval(stack);
		assertEquals(3, stack.pop(), DELTA);
	}

	@Test
	public void multiDivision() {
		stack.push(50);
		stack.push(2);
		stack.push(5);
		division.eval(stack);
		assertEquals(50 / 2 / 5, stack.pop(), DELTA);
	}

	@Test(expected = IllegalArgumentException.class)
	public void divisionNAriosWithNoArgument() {
		division.eval(stack);
	}
}
