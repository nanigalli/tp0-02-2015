package ar.fiuba.tdd.tp0.Operator;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import ar.fiuba.tdd.tp0.StackNumbers;

public class GetNumberTest {

	private static final double DELTA = 0.00001;
	private final StackNumbers stack = new StackNumbers();

	@Test
	public void getNumber() {
		GetNumber getNumber = new GetNumber("5");
		getNumber.eval(stack);
		assertEquals(5, stack.pop(), DELTA);
	}

	@Test
	public void getNumberNegative() {
		GetNumber getNumber = new GetNumber("-5");
		getNumber.eval(stack);
		assertEquals(-5, stack.pop(), DELTA);
	}

	@Test(expected = IllegalArgumentException.class)
	public void getNotNumber() {
		GetNumber getNumber = new GetNumber("+");
		getNumber.eval(stack);
	}

	@Test(expected = IllegalArgumentException.class)
	public void NullNumber() {
		new GetNumber(null);
	}

	@Test(expected = IllegalArgumentException.class)
	public void getNumberEmpty() {
		GetNumber getNumber = new GetNumber("");
		getNumber.eval(stack);
	}

}
