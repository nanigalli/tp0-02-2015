package ar.fiuba.tdd.tp0.Operator;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import ar.fiuba.tdd.tp0.StackNumbers;

public class SubstractionNAriosTest {
	private static final double DELTA = 0.00001;
	private final OperatorNAriosWithReverseStack subtraction = new OperatorNAriosWithReverseStack((x, y) -> x - y);
	private final StackNumbers stack = new StackNumbers();

	@Test
	public void subtraction() {
		stack.push(5);
		stack.push(2);
		subtraction.eval(stack);
		assertEquals(5 - 2, stack.pop(), DELTA);
	}

	@Test
	public void subtractionNegativeNumbers() {
		stack.push(3);
		stack.push(-2);
		subtraction.eval(stack);
		assertEquals(3 + 2, stack.pop(), DELTA);
	}

	@Test(expected = IllegalArgumentException.class)
	public void subtractionWithoutStack() {
		subtraction.eval(null);
	}

	@Test
	public void incompleteExpression() {
		stack.push(3);
		subtraction.eval(stack);
		assertEquals(3, stack.pop(), DELTA);
	}

	@Test
	public void multiSubtraction() {
		stack.push(5);
		stack.push(2);
		stack.push(1);
		subtraction.eval(stack);
		assertEquals(5 - 2 - 1, stack.pop(), DELTA);
	}

	@Test(expected = IllegalArgumentException.class)
	public void subtractionWithNoArgument() {
		subtraction.eval(stack);
	}
}
